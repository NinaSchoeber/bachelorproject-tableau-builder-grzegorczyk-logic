package Functionality;

import ConnectiveClasses.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import de.nixosoft.jlr.*;

/**
 *
 * @author nina
 */
public class Builder {
    private boolean infiniteTree;
    private ArrayList<ArrayList<Formula>> counterModelFormulas;
    private ArrayList<String> counterModelRelations;
    
    public Builder(){
        infiniteTree = false;
    }
    
    private int findMainConnective(String s){
        int i = 0;
        for(i = 0; i<s.length();i++){
            if(s.charAt(i)=='(' || s.charAt(i)=='\u22A5' || ((s.charAt(i)>=48 && s.charAt(i)<=57) || (s.charAt(i)>=65 && s.charAt(i)<=90) || (s.charAt(i)>=97 && s.charAt(i)<=122))){
                break;
            }
        }
        if(i==s.length()){
            System.out.println("No atoms: " + s);
            System.exit(0);
        }
        int index = 0; //index of last letter atom or closing bracket
        if(s.charAt(i)=='('){
            index = findClosingBracket(s.substring(i)) + i;
        } else if((s.charAt(i)>=48 && s.charAt(i)<=57) || (s.charAt(i)>=65 && s.charAt(i)<=90) || (s.charAt(i)>=97 && s.charAt(i)<=122)){
            while (i<s.length()-1 && ((s.charAt(i)>=48 && s.charAt(i)<=57) || (s.charAt(i)>=65 && s.charAt(i)<=90) || (s.charAt(i)>=97 && s.charAt(i)<=122))){
                i+=1;
            }
            index = i;
        } else if(s.charAt(i)=='\u22A5'){
            index = i+1;
        }
        
        if(index>=s.length()-1){
            return 0;
        } else {
            return index;
        }
        
    }
    
    private int findClosingBracket(String s){
        int strCnt = 1;
        int index = 1;
        while(strCnt!=0 && index<s.length()){
            if(s.charAt(index)=='('){
                strCnt += 1;
            } else if(s.charAt(index)==')'){
                strCnt -= 1;
            }
            index += 1;
        }
        if(strCnt!=0){
            System.out.println("No corresponding closing bracket: " +s);
            System.exit(0);
        }
        return index;
    }
    public Formula parse(String s) throws IllegalArgumentException{
        if (s.length()<1){
            throw new IllegalArgumentException("Not a valid formula: " + s);
        }
        int conIndex = this.findMainConnective(s);
        if(conIndex==0){
            if(s.charAt(0)=='('){ //only a formula between brackets
                return parse(s.substring(1,s.length()-1)); 
            } else if(s.charAt(0)=='\u22A5'){ //absurd
                return new Absurd(false);
            } else if ((s.charAt(0)>=48 && s.charAt(0)<=57) || (s.charAt(0)>=65 && s.charAt(0)<=90) || (s.charAt(0)>=97 && s.charAt(0)<=122)){ //only a name 
                return new Atom(s);
            }
            switch(s.charAt(0)){
                case '\u00AC':
                    try{
                        return new Negation(parse(s.substring(1)));
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case '\u25FB':
                    try{
                        return new Necessity(parse(s.substring(1)));
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                case '\u25C7':
                    try{
                        return new Possibility(parse(s.substring(1)));
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                default:
                    System.out.println("Unexpected character: " + s.charAt(0));
            }
        } else {
            Formula arg1 = parse(s.substring(0,conIndex));
            Formula arg2 = parse(s.substring(conIndex+1));
            switch(s.charAt(conIndex)){
                case '\u2227':
                    try{
                        return new Conjunction(arg1,arg2);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                case '\u2228':
                    try{
                        return new Disjunction(arg1,arg2);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                case '\u2192':
                    try{
                        return new Conditional(arg1,arg2);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                case '\u2194':
                    try{
                        return new Biconditional(arg1,arg2);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                default:
                    throw new IllegalArgumentException("Not a valid formula, here: " + s);
            }
        }
        return new Formula();
    }

    private boolean contradiction(Tree checkNode){
        Tree tChecking;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(checkNode);
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(!tChecking.getClosed()){
                if(tChecking.getFormula().equalFormulas((new Negation(checkNode.getFormula()))) || (new Negation(tChecking.getFormula())).equalFormulas(checkNode.getFormula())){
                    if(tChecking.getWorld().getWorldNum()==checkNode.getWorld().getWorldNum()){
                        tChecking.setClosed();
                        return true;
                    }
                }
                if(tChecking.getLeftChild()!=null){
                    toCheck.add(tChecking.getLeftChild());
                    if(tChecking.getRightChild()!=null){
                        toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                    }
                }
            }
        }
        return false;
    }
    
    private void close(Tree t){
        //uses BFS
        boolean changed;
        Tree tChecking;
        changed = false;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(t);
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(!tChecking.getClosed()){
                if(!(tChecking.getFormula() instanceof WorldRelation) && this.contradiction(tChecking)){
                    changed = true;
                }
                if(tChecking.getLeftChild()!=null){
                    toCheck.add(tChecking.getLeftChild());
                    if(tChecking.getRightChild()!=null){
                        toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                    }
                }
            }
        }
        while(changed){
            toCheck.add(t);    
            changed = false;
            while(!toCheck.isEmpty()){
                tChecking = toCheck.remove(0);
                if(tChecking.getClosed()){
                    continue; //do not check below this node, but do finish queue
                }
                if(tChecking.getLeftChild()!=null && tChecking.getLeftChild().getClosed()){
                    if(tChecking.getRightChild()==null || tChecking.getRightChild().getClosed()){
                        tChecking.setClosed();
                        changed = true;
                        continue;
                    }
                }
                if(tChecking.getLeftChild()!=null){
                    toCheck.add(tChecking.getLeftChild());
                    if(tChecking.getRightChild()!=null){
                        toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                    }
                }
            }
        }
    }
    
    private boolean findRelation(Tree checkNode){ //finds jrk for every irj on the branch --> might be going wrong for multiple jrk
        boolean added = false;
        if(checkNode.getFormula().getFirstWorld()==checkNode.getFormula().getSecondWorld()){
            return false;
        }
        Tree tChecking;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(checkNode);
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(tChecking.getClosed()){
                continue;
            }
            if((tChecking.getFormula() instanceof WorldRelation) && tChecking.getFormula().getFirstWorld()==checkNode.getFormula().getSecondWorld()){
                if(tChecking.getFormula().getFirstWorld()!=tChecking.getFormula().getSecondWorld()){
                    if(tChecking.addNode(new Tree(new WorldRelation(checkNode.getFormula().getFirstWorld(),tChecking.getFormula().getSecondWorld()), tChecking.getWorld(), tChecking.getConstants()),checkNode)){
                        added = true;
                    }
                }
            }
            if(tChecking.getLeftChild()!=null){
                toCheck.add(tChecking.getLeftChild());
                if(tChecking.getRightChild()!=null){
                    toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                }
            }
            
        }
        return added;
    }
    
    public boolean updateTransitivity(Tree t){
        Tree tChecking;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(t);
        boolean addedRelation = false;
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(tChecking.getClosed()){
                continue;
            }
            if((tChecking.getFormula() instanceof WorldRelation)){
                if(this.findRelation(tChecking)){
                    addedRelation = true;
                }
            }
            if(tChecking.getLeftChild()!=null){
                toCheck.add(tChecking.getLeftChild());
                if(tChecking.getRightChild()!=null){
                    toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                }
            }
        }
        return addedRelation;
    }
    
    private int formulaValue(Tree t){ //to implement hierarchy of formulas (first the ones that do not split)
        //first non splitting, then splitting, then necessities and negations of possibilities
        if(t.getFormula() instanceof Necessity || (t.getFormula() instanceof Negation && t.getFormula().getFormula() instanceof Possibility)){
            return 1;
        }
        if(t.getFormula() instanceof Disjunction ||
                t.getFormula() instanceof Conditional ||
                t.getFormula() instanceof Biconditional ||
                (t.getFormula() instanceof Negation && t.getFormula().getFormula() instanceof Conjunction)){
            return 2;
        } 
        return 3; //nonsplitting
    }
    
    private boolean infiniteTree(Tree t, ArrayList<ArrayList<Formula>> worldKnowledge, ArrayList<String> wRel){
        //build knowledge base for each world in branch
        for(int i=0; i<worldKnowledge.size()-1; i++){
            for(int j=i+1; j<worldKnowledge.size(); j++){
                //compare two worlds iff there is a relation between them
                if(!wRel.contains(i+"r"+j)){
                    continue;
                }
                if(worldKnowledge.get(i).size()>0 && worldKnowledge.get(j).size()>0 && worldKnowledge.get(i).containsAll(worldKnowledge.get(j)) && worldKnowledge.get(j).containsAll(worldKnowledge.get(i))){
                    this.infiniteTree = true;
                    this.counterModelFormulas = new ArrayList<>();
                    for(ArrayList<Formula> ar: worldKnowledge){
                        this.counterModelFormulas.add(new ArrayList<>(ar));
                    }
                    this.counterModelRelations = new ArrayList<>(wRel);
                    return true;
                }
            }
        }
        ArrayList<ArrayList<Formula>> newWK = new ArrayList<>();
        ArrayList<String> newWRel = new ArrayList<>(wRel);
        for(ArrayList<Formula> ar: worldKnowledge){
            newWK.add(new ArrayList<>(ar));
        }
        if(t.getWorld().getWorldNum()>=worldKnowledge.size()){
            newWK.add(new ArrayList<>());
        }
        if (!(t.getFormula() instanceof WorldRelation)){
            newWK.get(t.getWorld().getWorldNum()).add(t.getFormula());
        } else{
            newWRel.add(t.getFormula().toString());
        }
            
        if(t.getLeftChild()!=null){
            if(t.getRightChild()!=null){
                return infiniteTree(t.getLeftChild(), newWK, newWRel) || infiniteTree(t.getRightChild(),newWK, newWRel);
            }
            return infiniteTree(t.getLeftChild(), newWK, newWRel);
        }
        return false;
    }
       
    private void apply(Tree t){
        //BFS over nodes, apply when not applied yet
        boolean changed;
        Tree tChecking;
        Tree toApply;
        ArrayList<Tree> toCheck= new ArrayList<>();
        ArrayList<Tree> recurringFormulas = new ArrayList<>();
        ArrayList<ArrayList<Formula>> worldKnowledge = new ArrayList<>();
        worldKnowledge.add(new ArrayList<>());
        do{
            toApply = null;
            changed = false;
            toCheck.clear();
            toCheck.add(t);
            while(!toCheck.isEmpty()){
                tChecking = toCheck.remove(0);
                if(tChecking.getClosed()){
                    continue;
                }
                if(!(tChecking.getFormula() instanceof WorldRelation) && !(tChecking.getFormula() instanceof Atom) && !tChecking.getApplied()){
                    if(toApply ==null){
                        toApply = tChecking;
                    } else if(formulaValue(toApply)<=formulaValue(tChecking)){
                        toApply = tChecking;
                    }
                }
                if(tChecking.getLeftChild()!=null){
                    toCheck.add(tChecking.getLeftChild());
                    if(tChecking.getRightChild()!=null){
                        toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                    }
                }
            }
            if(toApply!=null){
                if((toApply.getFormula() instanceof Necessity) || ((toApply.getFormula() instanceof Negation) && (toApply.getFormula().getFormula() instanceof Possibility)) ){
                    recurringFormulas.add(toApply);
                }
                toApply.apply(t);
                changed = true;
            }
            this.close(t);
            if(t.getClosed()){
                break;
            }          
            ArrayList<Tree> toRemove = new ArrayList<>();
            for(Tree tRecur : recurringFormulas){
                if(tRecur.getClosed()){
                    toRemove.add(tRecur);
                } else if (tRecur.apply(t)){
                    changed = true;
                }
            }
            recurringFormulas.removeAll(toRemove);
            
            if(infiniteTree(t,worldKnowledge, new ArrayList<>())){
                break;
            }
            this.close(t);
            if(t.getClosed()){
                break;
            }
            if(this.updateTransitivity(t)){
                changed = true;
            }
        } while(changed && !t.getClosed());
    }
    
   //finds a countermodel, but not necessarily the shortest one
    private String counterModel(Tree t){
        ArrayList<String> worlds = new ArrayList<>();
        ArrayList<String> relations = new ArrayList<>();
        ArrayList<String> valuations = new ArrayList<>();
        //DFS through tree, only writing down non closed, stopping when leaf
        //find all irj and atoms and negations of atoms
        if(this.counterModelFormulas!=null){
            for(int i = 0; i<this.counterModelFormulas.size(); i++){
                for(Formula f: this.counterModelFormulas.get(i)){
                    if(f instanceof Atom || (f instanceof Negation && f.getFormula() instanceof Negation && f.getFormula().getFormula() instanceof Atom)){
                        String val = "v_{w_{" + i + "}}(" + f + ") = 1";
                        if(!valuations.contains(val)){
                            valuations.add(val);
                        }
                    } else if(f instanceof Negation && f.getFormula() instanceof Atom){
                        String val = "v_{w_{" + i + "}}(" + f.getFormula() + ") = 0";
                        if(!valuations.contains(val)){
                            valuations.add(val);
                        }
                    }
                }
            }
            worlds.add("w_{0}");
            for(String rel: this.counterModelRelations){
                relations.add(rel);
                String[] relWorlds = rel.split("r");
                if(!relWorlds[0].equals(relWorlds[1])){
                    String w = "w_{" + relWorlds[1] + "}";
                    if(!worlds.contains(w)){
                        worlds.add(w);
                    }
                }
            }
        } else{ 
            Tree tChecking;
            ArrayList<Tree> toCheck= new ArrayList<>();
            toCheck.add(t);
            worlds.add(t.getWorld().toString());
            while(!toCheck.isEmpty()){
                tChecking = toCheck.remove(toCheck.size()-1); //DFS with preference for right
                if(tChecking.getClosed()){ //branch has to be complete and not closed --> TODO or infinite! || !tChecking.getApplied()
                    continue;
                }
                if(tChecking.getFormula() instanceof WorldRelation){
                    relations.add(tChecking.getFormula().toString());
                    if(tChecking.getFormula().getFirstWorld()!=tChecking.getFormula().getSecondWorld()){
                        String w = tChecking.getFormula().getSecondWorld().toString();
                        if(!worlds.contains(w)){
                            worlds.add(w);
                        }
                    }
                } else if(tChecking.getFormula() instanceof Atom || (tChecking.getFormula() instanceof Negation && tChecking.getFormula().getFormula() instanceof Negation && tChecking.getFormula().getFormula().getFormula() instanceof Atom)){
                    String val = "v_{" + tChecking.getWorld() + "}(" + tChecking.getFormula() + ") = 1";
                    if(!valuations.contains(val)){
                        valuations.add(val);
                    }
                } else if(tChecking.getFormula() instanceof Negation && tChecking.getFormula().getFormula() instanceof Atom){
                    String val = "v_{" + tChecking.getWorld() + "}(" + tChecking.getFormula().getFormula() + ") = 0";
                    if(!valuations.contains(val)){
                        valuations.add(val);
                    }
                }

                if(tChecking.getLeftChild()!=null){
                    if(tChecking.getRightChild()!=null){
                        toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                    }
                    toCheck.add(tChecking.getLeftChild());
                } else { // stop at leaf
                    break;
                }
            }
        }
        String model = "";
        //worlds
        model += "$W = " + (char) 92 + "{" ;
        for(int i = 0; i<worlds.size(); i++){
            model += worlds.get(i);
            if(i<worlds.size()-1){
                model += ",";
            }
        }
        model += (char) 92 + "}$\\\\";
        //relations
        model += "$R = " + (char) 92 + "{";
        for(int i =0; i<relations.size(); i++){
            String[] relWorlds = relations.get(i).split("r");
            model += "<w_{" + relWorlds[0] + "},w_{" + relWorlds[1] +"}>";
            if(i<relations.size()-1){
                model += ",";
            }
        }
        model += (char) 92 + "}$\\\\";
        //atoms
        for(int i=0; i<valuations.size(); i++){
            model+= "$" + valuations.get(i) + "$";
            if(i<valuations.size()-1){
                model += ", ";
            }
        }
        return model;
    }
    
    private void writeToFile(Tree t, File dir){
        String time = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
        String fileName = dir.getPath() + "/" + time + ".tex";
        String header = "\\documentclass{article}" +
                        "\\usepackage[a2paper]{geometry}" +
                        "\\usepackage{forest}\n" +
                        "\\usepackage{amssymb}\n" +
                        "\\title{Tableau for $" + t.getFormula() + "$}\n" +
                        "\\date{}\n" +
                        "\n" +
                        "\\begin{document}\n" +
                        "\\maketitle\n" +
                        "\\begin{center}\n" +
                        "\\begin{forest}\n";
        String close = "\\end{forest}\\vspace{5 mm}\n" +
                        (t.getClosed() ? "\\\\This tableau is closed." : "\\\\This tableau is open "+ (this.infiniteTree ? " but not complete.\\\\ Tableau was cut off because of an infinite sequence." : "and complete. \\\\") + "\\vspace{5 mm} \\\\ Countermodel\\\\" + counterModel(t))+
                        "\\end{center}\n"+
                        "\\end{document}";
        try {
            //create tex file
            File file = new File(fileName);
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(header);
            bw.write(t.toString());
            bw.write(close);
            bw.close();
            fw.close();
            
            //convert to pdf
            File location = dir;
            JLRGenerator pdfGen = new JLRGenerator();
            pdfGen.generate(file,location, location);
            File pdf = pdfGen.getPDF();
            
            //open pdf
            JLROpener.open(pdf);
        } catch (IOException ex) {
            Logger.getLogger(Builder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void buildTree(String formula, File dir){
        infiniteTree = false;
        counterModelFormulas = null;
        counterModelRelations = null;
        formula = formula.replaceAll("\\s+",""); //strip whitespace
        Formula f = this.parse(formula);
        World w = new World(0);
        Tree t = new Tree(f,w,new Constants());
        t.addNode(new Tree(new WorldRelation(w,w),w,t.getConstants()),t);
        this.apply(t);
        this.close(t);
        this.writeToFile(t, dir);
    }
}
