package Functionality;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;

/**
 *
 * @author nina
 */
public class DirectoryChooser extends JPanel implements ActionListener {
    JButton select;
    JLabel path;
    JFileChooser chooser;
    String choosedir;
    File chosenDir;
    
    public DirectoryChooser(){
        super();
        chosenDir = null;
        select = new JButton("Select directory");
        select.addActionListener(this);
        path = new JLabel((chosenDir==null ? "No directory selected" : chosenDir.getPath()));
        this.add(select);
        this.add(path);
    }
    
    public File getDir(){
        return chosenDir;
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle(choosedir);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if(chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            chosenDir = chooser.getCurrentDirectory();
        }
        path.setText((chosenDir==null ? "No directory selected" : chosenDir.getPath()));
    }
}
