package Functionality;

import ConnectiveClasses.*;
import java.util.ArrayList;

/**
 *
 * @author nina
 */
public class World {
    private int num;
    
    public World(int n){
        this.num = n;
    }

    public int getWorldNum(){
        return num;
    }
        
    public String toString(){
        return "w_{" + this.num + "}";
    }
}
