package Functionality;

import ConnectiveClasses.*;
import java.util.ArrayList;

/**
 *
 * @author nina
 */
public class Tree {
    private Formula f;
    private World w;
    private Tree t1;
    private Tree t2;
    private boolean applied;
    private boolean closed;
    private Constants c;
   
    public Tree(Formula form, World world, Constants con){
        this.f = form;
        this.w = world;
        t1 = null;
        t2 = null;
        applied = false;
        closed = false;
        this.c = con;
    }
    
    public Tree(Tree t){
        this.f = t.getFormula();
        this.w = t.getWorld();
        if(t.getLeftChild()!=null){
            t1 = new Tree(t.getLeftChild());
            if(t.getRightChild()!=null){
                t2 = new Tree(t.getRightChild());
            } else {
                t2 = null;
            }
        } else{
            t1 = null;
            t2 = null;
        }
        applied = false;
        closed = false;
        this.c = new Constants(t.getConstants());
    }
    
    public Constants getConstants(){
        return c;
    }
        
    public Formula getFormula(){
        return this.f;
    }
    
    public boolean getClosed(){
        return this.closed;
    }
    
    public void setClosed(){
        this.addNode(new Tree(new Absurd(true), w, c), this);
        this.closed = true;
    }
             
    public void setChildren(Tree new1, Tree new2){
        if(t1!=null){
            System.exit(0);
        } else {
            t1 = new1;
            t2 = new2;
        }
    }
    
    public void newChild(Tree newTree){
        if(t1!=null){
            System.exit(0);
        } else {
            t1 = newTree;
        }
    }
    
    public boolean addNode(Tree newTree,Tree entireTree){
        boolean added = false;
        boolean foundNode = false;
        Tree tChecking;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(entireTree);
        //check if the new node already exists above this node 
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(tChecking.getClosed()){
                continue;
            }
            if(tChecking.getFormula().equalFormulas(newTree.getFormula()) && tChecking.getWorld()==newTree.getWorld()){
                continue;
            }
            if(tChecking == this){
                //found this node without encountering the new node
                toCheck.clear(); //continue only from this node
                foundNode = true;
            }
            if(tChecking.getLeftChild()!=null){
                toCheck.add(tChecking.getLeftChild());
                if(tChecking.getRightChild()!=null){
                    toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                }
            } else if(foundNode){
                added = true;
                tChecking.newChild(new Tree(newTree));
            }
        }        
        return added;
    }

    public boolean splitBranch(Tree new1, Tree new2){
        Tree tChecking;
        boolean added = false;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(this);
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(toCheck.size()-1);
            if(tChecking.getClosed()){
                continue;
            }
            if(tChecking.getLeftChild()!=null){
                toCheck.add(tChecking.getLeftChild());
                if(tChecking.getRightChild()!=null){
                    toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                }
            } else{
                added = true;
                tChecking.setChildren(new Tree(new1),new Tree(new2));
            }
        }
        return added;
    }
    
    public World getWorld(){
        return w;
    }
    
   
    public boolean apply(Tree entireTree){
        this.applied = true;
        return this.f.Apply(this,entireTree);
    }
    
    public boolean getApplied(){
        return this.applied;
    }
    
    public Tree getLeftChild(){
        return this.t1;
    }
    public Tree getRightChild(){
        return this.t2;
    }
    
    @Override
    public String toString(){
        return "[$" + this.f + (!(this.f instanceof WorldRelation || this.f instanceof Absurd) ? " . " + w.getWorldNum() : "") + "$ " + (t1==null? "" : t1.toString()) + " " + (t2==null? "" : t2.toString()) + " ]";
    }
}