package Functionality;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;

/**
 *
 * @author nina
 */
public class Screen extends JFrame{
    Builder b;
    String filePath;
    String formula;
    JTextField text;
    JButton enter;
    JButton not;
    JButton or;
    JButton and;
    JButton cond;
    JButton bicond;
    JButton nec;
    JButton pos;
    JButton absurd;
    JPanel connectives;
    JPanel input;
    
    DirectoryChooser panel;
    public Screen(){
        super("Tableau Builder");
        formula = "";
        panel = new DirectoryChooser();
        text = new JTextField(30);
        enter = new JButton("Build tableau");
        not = new JButton("\u00AC");
        or = new JButton("\u2228");
        and = new JButton("\u2227");
        cond = new JButton("\u2192");
        bicond = new JButton("\u2194");
        nec = new JButton("\u25FB");
        pos = new JButton("\u25C7");
        absurd = new JButton("\u22A5");
        this.setSize(400,180);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        input = new JPanel(new FlowLayout());
        input.add(text);
        input.add(enter);
        connectives = new JPanel(new FlowLayout());
        connectives = new JPanel();
        connectives.add(not);
        connectives.add(or);
        connectives.add(and);
        connectives.add(cond);
        connectives.add(bicond);
        connectives.add(nec);
        connectives.add(pos);
        connectives.add(absurd);
        
        this.add(input, BorderLayout.CENTER);
        this.add(connectives,BorderLayout.NORTH);
        this.add(panel, BorderLayout.SOUTH);
    }
    
    public static void main(String [] args){
        Screen s = new Screen();
        s.b = new Builder();
        s.addListeners();
    }
    
    private void addCharacter(String chr){
        formula = text.getText();
        formula = formula + chr;
        text.setText(formula);
    }
    
    private void addListeners(){
        enter.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                formula = text.getText();
                if(formula.length()==0){
                    JOptionPane.showMessageDialog(Screen.this,"No formula given", "Formula error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                //pass to builder
                File dir = panel.getDir();
                if(dir==null){
                    JOptionPane.showMessageDialog(Screen.this,"No directory selected", "Directory error", JOptionPane.ERROR_MESSAGE);
                } else{
                    b.buildTree(formula, dir);
                }
            }
        });
        text.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                formula = text.getText();
            }
        });
        not.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u00AC");
                text.requestFocus();
            }
        });
        or.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u2228");
                text.requestFocus();
            }
        });
        and.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u2227");
                text.requestFocus();
            }
        });        
        cond.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u2192");
                text.requestFocus();
            }
        });  
        bicond.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u2194");
                text.requestFocus();
            }
        });      
        nec.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u25FB");
                text.requestFocus();
            }
        });  
        pos.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u25C7");
                text.requestFocus();
            }
        });  
        absurd.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addCharacter("\u22A5");
                text.requestFocus();
            }
        });          
        
    }
    
    public String getFormula(){
        if(formula!=null){
            return formula;
        } else {
            return " ";
        }
    }
}
