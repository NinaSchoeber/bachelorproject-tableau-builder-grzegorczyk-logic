package ConnectiveClasses;

import Functionality.*;
import java.util.ArrayList;

/**
 *
 * @author nina
 */
public class Formula {
    public boolean Apply(Tree node,Tree t){
        return true;
    }
    public Formula getFormula1(){
        return new Formula();
    }
    
    public Formula getFormula2(){
        return new Formula();
    }
    
    public Formula getFormula(){
        return new Formula();
    }
    
    public String getString(){
        return "";
    }
    public boolean Negate(Tree node,Tree t){
        return true;
    }
    public World getFirstWorld(){
        return new World(-999);
    }
    public World getSecondWorld(){
        return new World(-999);
    }
    
    @Override
    public String toString(){
        return " ";
    }
    
    public boolean equalFormulas(Formula x){
        return x.toString().equals(this.toString());
    }
    
    @Override
    public boolean equals(Object o){
        return o.toString().equals(this.toString());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }
}