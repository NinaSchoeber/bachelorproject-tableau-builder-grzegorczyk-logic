package ConnectiveClasses;

import Functionality.*;

/**
 *
 * @author nina
 */
public class Negation extends Formula{
    Formula f;
    public Negation(Formula form){
        this.f = form;
    }
    
    public Formula getFormula(){
        return f;
    }
    
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        return f.Negate(t, entireTree);
    }
    
    @Override
    public boolean Negate(Tree t, Tree entireTree){
        return f.Apply(t, entireTree);
    }
    
    @Override
    public String toString(){
        return "\\neg " + f;
    }
    
}
