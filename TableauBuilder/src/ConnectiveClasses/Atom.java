package ConnectiveClasses;

import Functionality.*;

/**
 *
 * @author nina
 */
public class Atom extends Formula{
    String prop;
    public Atom(String s){
        this.prop = s;
    }
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        return true;
    }
    @Override
    public boolean Negate(Tree t, Tree entireTree){
        return true;
    }
    @Override
    public String toString(){
        return prop;
    }
    
    public String getString(){
        return prop;
    }
   
}
