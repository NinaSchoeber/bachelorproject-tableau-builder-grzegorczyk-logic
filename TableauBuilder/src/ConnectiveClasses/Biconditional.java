package ConnectiveClasses;

import Functionality.*;

/**
 *
 * @author nina
 */
public class Biconditional extends Formula{
    Formula f1;
    Formula f2;
    public Biconditional(Formula form1, Formula form2){
        this.f1 = form1;
        this.f2 = form2;
    }
    
    public Formula getFormula1(){
        return f1;
    }
    public Formula getFormula2(){
        return f2;
    }
    
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        Tree newLeft = new Tree(f1,t.getWorld(), t.getConstants());
        newLeft.addNode(new Tree(f2,t.getWorld(), t.getConstants()), newLeft);
        Tree newRight = new Tree(new Negation(f1),t.getWorld(), t.getConstants());
        newRight.addNode(new Tree(new Negation(f2), t.getWorld(), t.getConstants()),newRight);
        return t.splitBranch(newLeft, newRight);
    }
    @Override
    public boolean Negate(Tree t, Tree entireTree){
        Tree newLeft = new Tree(f1,t.getWorld(), t.getConstants());
        newLeft.addNode(new Tree(new Negation(f2),t.getWorld(), t.getConstants()),newLeft);
        Tree newRight = new Tree(new Negation(f1),t.getWorld(), t.getConstants());
        newRight.addNode(new Tree(f2, t.getWorld(), t.getConstants()),newRight);
        return t.splitBranch(newLeft, newRight);
    }
    
    @Override
    public String toString(){
        return "("+ f1 + "\\leftrightarrow " + f2 + ")";
    }
    
}
