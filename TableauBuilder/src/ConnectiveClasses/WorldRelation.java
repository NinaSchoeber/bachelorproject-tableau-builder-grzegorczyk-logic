package ConnectiveClasses;

import Functionality.*;

/**
 *
 * @author nina
 */
public class WorldRelation extends Formula{
    World i;
    World j;
    public WorldRelation(World a, World b){
        this.i = a;
        this.j = b;
    }
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        return true;
    }
    @Override
    public boolean Negate(Tree t, Tree entireTree){
        return true;
    }
    public World getFirstWorld(){
        return this.i;
    }
    public World getSecondWorld(){
        return this.j;
    }
    
    @Override
    public String toString(){
        return i.getWorldNum() +"r"+j.getWorldNum();
    }
   
}
