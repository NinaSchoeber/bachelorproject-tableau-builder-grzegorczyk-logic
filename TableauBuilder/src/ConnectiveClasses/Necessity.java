package ConnectiveClasses;

import Functionality.*;
import java.util.ArrayList;

/**
 *
 * @author nina
 */
public class Necessity extends Formula{
    Formula f;
    public Necessity(Formula form){
        this.f = form;
    }
    
    @Override
    public Formula getFormula(){
        return f;
    }
        
    @Override
    public boolean Apply(Tree node, Tree t){ //node is where necessity occurs, t is entire tree
        boolean changed = false;
        //searching in two directions (because irj can be above necessity)
        Tree tChecking;
        ArrayList<Tree> toCheck = new ArrayList<>();
        toCheck.add(node); //starting at necessity, adding at irj
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(tChecking.getClosed()){
                continue;
            }
            if((tChecking.getFormula() instanceof WorldRelation) && tChecking.getFormula().getFirstWorld()==node.getWorld()){
                if(tChecking.addNode(new Tree(f,tChecking.getFormula().getSecondWorld(),tChecking.getConstants()),t)){
                    changed = true;                 
                }
            }
            if(tChecking.getLeftChild()!=null){
                toCheck.add(tChecking.getLeftChild());
                if(tChecking.getRightChild()!=null){
                    toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                }
            }
            
        }
        
        //starting at irj, continuing to necessity
        toCheck.clear(); //although toCheck should always be empty at this point
        toCheck.add(t); //starting at irj, adding at necessity
        ArrayList<Tree> findNext = new ArrayList<>();
        while(!toCheck.isEmpty()){
            tChecking = toCheck.remove(0);
            if(tChecking.getClosed()){
                continue;
            }
            if(tChecking.getFormula() instanceof WorldRelation && tChecking.getFormula().getFirstWorld()==node.getWorld()){
                findNext.add(tChecking);
                Tree tFinding;
                while(!findNext.isEmpty()){
                    tFinding = findNext.remove(0);
                    if(tFinding.getClosed()){
                        continue;
                    }
                    if(tFinding.getFormula().equalFormulas(node.getFormula()) && tFinding.getWorld()==node.getWorld()){
                        if(tFinding.addNode(new Tree(f,tChecking.getFormula().getSecondWorld(),tFinding.getConstants()),t)){
                            changed = true;
                            continue;
                        }
                    }
                    if(tFinding.getLeftChild()!=null){
                        findNext.add(tFinding.getLeftChild());
                        if(tFinding.getRightChild()!=null){
                            findNext.add(tFinding.getRightChild()); //right can never contain a tree if left does not
                        }
                    }
                }
            }
            if(tChecking.getLeftChild()!=null){
                toCheck.add(tChecking.getLeftChild());
                if(tChecking.getRightChild()!=null){
                    toCheck.add(tChecking.getRightChild()); //right can never contain a tree if left does not
                }
            }
            
        }
        return changed;
    }
    
    @Override
    public boolean Negate(Tree node, Tree t){
        World w = new World(t.getConstants().newWorld());
        boolean t1 = node.addNode(new Tree(new WorldRelation(node.getWorld(),w),w,node.getConstants()),t);
        boolean t2 = node.addNode(new Tree(new WorldRelation(w,w),w,node.getConstants()),t);
        boolean t3 = node.addNode(new Tree(new Negation(f),w,node.getConstants()),t);
        boolean t4 = node.addNode(new Tree(new Necessity(new Conditional(f,new Necessity(f))),w,node.getConstants()),t);
        return t1 || t2 || t3 || t4;
    }
    
    @Override
    public String toString(){
        return "\\square " + f;
    }
}
