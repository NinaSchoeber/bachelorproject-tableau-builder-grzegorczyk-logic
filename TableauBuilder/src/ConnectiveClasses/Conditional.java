package ConnectiveClasses;

import Functionality.*;

/**
 *
 * @author nina
 */
public class Conditional extends Formula{
    Formula f1;
    Formula f2;
    public Conditional(Formula form1, Formula form2){
        this.f1 = form1;
        this.f2 = form2;
    }
    
    public Formula getFormula1(){
        return f1;
    }
    public Formula getFormula2(){
        return f2;
    }
    
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        return t.splitBranch(new Tree(new Negation(f1),t.getWorld(),t.getConstants()),new Tree(f2,t.getWorld(),t.getConstants()));
    }
    @Override
    public boolean Negate(Tree t, Tree entireTree){
        boolean t1 =  t.addNode(new Tree(f1,t.getWorld(),t.getConstants()),entireTree);
        boolean t2 = t.addNode(new Tree(new Negation(f2),t.getWorld(),t.getConstants()),entireTree);
        return t1 || t2;
    }
    
    @Override
    public String toString(){
        return "("+ f1 + "\\to " + f2 + ")";
    }
    
    
}
