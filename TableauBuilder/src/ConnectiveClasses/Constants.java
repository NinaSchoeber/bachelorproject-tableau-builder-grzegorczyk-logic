package ConnectiveClasses;

/**
 *
 * @author nina
 */
public class Constants {
    int worldCount;
    
    public Constants(){
        worldCount=0;
    }
    
    public Constants(Constants c){
        worldCount = c.getWC();
    }
    
    public int newWorld(){
        this.worldCount += 1;
        return this.worldCount;
    }
    
    public int getWC(){
        return worldCount;
    }
}
