package ConnectiveClasses;

import Functionality.*;

/**
 *
 * @author nina
 */
public class Possibility extends Formula{
    Formula f;
    public Possibility(Formula form){
        this.f = form;
    }
    @Override
    public Formula getFormula(){
        return f;
    }
    
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        Formula fNew = new Necessity(new Negation(this.f));
        return fNew.Negate(t, entireTree);
    }
    
    @Override
    public boolean Negate(Tree node,Tree t){
        Formula fNew = new Necessity(new Negation(this.f));
        return fNew.Apply(node, t);
    }
    
    @Override
    public String toString(){
        return "\\diamond " + f;
    }
        
}
