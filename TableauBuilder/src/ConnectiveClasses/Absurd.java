/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConnectiveClasses;

import Functionality.Tree;

/**
 *
 * @author nina
 */
public class Absurd extends Formula{
    private boolean isLeaf;
    public Absurd(boolean leaf){
        isLeaf = leaf;
    }
     
    @Override
    public boolean Apply(Tree t, Tree entireTree){
        t.setClosed();
        return true;
    }
       
    @Override
    public String toString(){
        if(isLeaf){
            return "\\times";
        } else {
            return "\\bot";
        }
    }
    
}
